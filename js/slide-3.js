
var points_1 = [{x:130, y:57},{x:170, y:100}, {x:122, y:140},
			  {x:131, y:183}, {x:126, y:238}, {x:161, y:326},
			  {x:172, y:375}, {x:157, y:456}, {x:189, y:510}];

var points_2 = [{x:200, y:92}, {x:160, y:30},
			  {x:105, y:40}, {x:87, y:82}, {x:125, y:114},
			  {x:130, y:153}, {x:166, y:204}, {x:137, y:236}
			  , {x:136, y:265}, {x:135, y:303}, {x:157, y:366}
			  , {x:175, y:401}, {x:157, y:460}, {x:200, y:500}];


var countTimer;
var restartTimer;

var kilometres;

$(document).ready(function()
{
	$("#slide-3 .switcher a").click(function()
	{
		var ind=$(this).index();
		$(".road-wrap").hide();
		$(".textslide3").hide();
		$("#slide-3 .switcher a").removeClass("active");
		$(this).addClass("active");
		$(".road-wrap").eq(ind).show();
		$(".textslide3").eq(ind).show();

		//preperSlide3();
		relLoda();
	});
});

function startSlide3()
{
	//var h = $("#slide-3 .road-wrap .road-white").css('height');
	//var l = $("#slide-3 .road-wrap .road-marker").css('left');
	moveMarker(0);
	countKilo (kilometres);
	relLoda();
}

function moveMarker(i)
{
	var ind=$("#slide-3 .switcher a.active").index();
	var roadWrap=$("#slide-3 .road-wrap").eq(ind);

	var points=(ind==0)? points_1 : points_2;

	var x1=parseInt(roadWrap.find(".road-marker").css("left"));
	var y1=parseInt(roadWrap.find(".road-marker").css("bottom"));

	var x2=points[i].x - 115;
	var y2=points[i].y-18;

	var length=Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
	var time=length*15;

	var heightK=(ind==0)? 28 : -20;

	if(ind==0)
	{
		roadWrap.find(".road-white").animate({"height":points[i].y-heightK +"px"},time,"linear");
	}
	else
	{
		
		
		if(i>=1)
			roadWrap.find(".road-white").animate({"height":points[i].y-heightK +"px"},time,"linear");
		else
			roadWrap.find(".road-white-1").animate({"height":"564px"},1500,"linear");	
	}

	roadWrap.find(".road-marker").animate({"left":points[i].x - 115+"px","bottom":points[i].y-18+"px"},time,"linear",function()
	{
		var next=i+1;
		if(next==points.length)
		{
			return;	
		} 
		else
		{
			moveMarker(next);			
		}
			
	});
}

function preperSlide3()
{
	$("#slide-3 .road-wrap .road-white").css("height","0");
	$("#slide-3 .road-wrap .road-white-1").css("height","450px");

	clearInterval(countTimer);
	$("#slide-3 .value").html("0");
	$("#slide-3 .road-wrap .road-white").stop(true,false);
	$("#slide-3 .road-wrap .road-white-1").stop(true,false);

	var ind=$("#slide-3 .switcher a.active").index();
	var points=(ind==0)? points_1 : points_2;
	kilometres=(ind==0)? 125 : 250;

	if (ind==1)
	{
		$("#slide-3 .road-wrap.road-2 .road-white").css("height","20px");
	}

	$("#slide-3 .road-wrap .road-marker").stop(true,false).css("left",points[0].x- 115+"px").css("bottom",points[0].y-18+"px");


}

function countKilo (to) {
			var $ele = $("#slide-3 .value"),
            num = parseInt($ele.html()),
            up = to > num,
            num_interval = Math.abs(num - to) / 90;
            num_interval=num_interval<1? 1 : num_interval;		
			
        var loop = function() {
            num = Math.floor(up ? num+num_interval: num-num_interval);
            if ( (up && num > to) || (!up && num < to) ) {
                num = to;
                clearInterval(countTimer);

            }
            $ele.html(num);
        }

		var  interval;

       	if(isSafari())
    	   	interval=(kilometres==125)? 60 : 83;
       	else
        	interval=(kilometres==125)? 57 : 80;


        countTimer = setInterval(loop, interval);
}

function relLoda()
{
	clearTimeout(restartTimer);
	
	var h = $("#slide-3 .road-wrap .road-white").css('height');
	var l = $("#slide-3 .road-wrap .road-marker").css('left');
	var t = jQuery('#slide-3').css('top');
	if(t!='0px')
	{
		return false;
	}
	else
	{
		$("#slide-3 .road-wrap .road-white").css('height',h);
		$("#slide-3 .road-wrap .road-marker").css('left',l);
		preperSlide3();
		moveMarker(0);
		countKilo (kilometres); 
	
		var ind=$("#slide-3 .switcher a.active").index();
		var timeOut;

		if(isSafari())
			timeOut=(ind==0)? 12000 : 15000;
		else
			timeOut=(ind==0)? 11000 : 14000;

		restartTimer= setTimeout('relLoda();', timeOut);
	}
	
}

function isSafari()
{
var ua = navigator.userAgent.toLowerCase(); 
 if (ua.indexOf('safari')!=-1){ 
   if(ua.indexOf('chrome')  > -1){
   	return false;
    }else{
    return true;
   }
  }

}