$(document).ready(function()
{
	var isiPad = navigator.userAgent.match(/iPad/i) != null;
    var isiPhone= navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i);
	if(isiPad)
		$('head').append('<meta name="viewport" content="width=device-width, initial-scale=0.8">');
	else if(isiPhone)
		$('head').append('<meta name="viewport" content="width=device-width, initial-scale=0.1">');
    
	//-----------mobile scroll-------------
    if(detectmob())
    {
    $("#wrapper").swipe({
        swipe:function(event, direction, distance, duration, fingerCount) {
            if(direction=="down")
            {
                $("body").trigger("mousewheel",1);
            }
            else if(direction=="up")
            {
                $("body").trigger("mousewheel",-1);  
            }
        }
    });
    }

	//-----------mobile scroll-------------
});

function detectmob() { 
 if( navigator.userAgent.match(/Android/i)
 || navigator.userAgent.match(/webOS/i)
 || navigator.userAgent.match(/iPhone/i)
 || navigator.userAgent.match(/iPad/i)
 || navigator.userAgent.match(/iPod/i)
 ){
    return true;
  }
 else {
    return false;
  }
}