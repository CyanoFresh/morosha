	var doc = $(document);
    var win = $(window);
    var videoProp = 1280/720;
    var curSlide = 1;
    var newSlide = 2;
    var slide1video = false;
    var slide2video = false;
    var slide2count = 0;
	var timeouts = new Array();
	var _loaded = 0;
	var animate_bars;

jQuery(function($){
	
    beforePageStart();
    if (get_cookie ( "pop" )=='no'){
		$(".pop").remove();
		$(".temno").remove();
	}
	win.load(function()
	{	                
		first_load();
	});
	  winResize();  
	
    
    /** Left navigation **/
    doc.find(".slide-menu-min a").click(function() {
        if($(this).hasClass('active')) return false;
        var wrap = doc.find('#wrapper');
        var el = $(this);
        var id = el.attr('id');
        $(".ul-top2").find("li").each(function(){
	        $(this).removeClass("act-li");
        });
        if (get_cookie ( "slide" )!=null){
        	var t1=get_cookie ( "slide" );
	    $(".ul-top2").find("."+t1).addClass("act-li");    
        } else {
        $(".ul-top2").find(".item-"+id).addClass("act-li");
        }
        var oldId = parseInt( wrap.attr('data-slide') );
        var newId = parseInt( id.replace('item-', '') );
		doc.find(".slide-menu-min a").removeClass("active");
                
        preperSlide(newId,function(){
            animateSlide(oldId,newId,function(){
                wrap.attr('data-slide',newId);
                startSlide(newId,function(){
                    wrap.removeClass('animate');
                    if(newId == 2){
                        
                    }
                    winResize();
                });
            });
        });
		return false;	
	});
	
	/** Top-2 navigation **/
    $(".ul-top2").find("li").click(function() {
        if($(this).hasClass('act-li')) return false;
        var wrap = doc.find('#wrapper');
        var el = $(this);
        var id = el.attr('class');
        $(".ul-top2").find("li").each(function(){
	        $(this).removeClass("act-li");
        });


        if(id!="item-1")
            $(".ul-top2").find("."+id).addClass("act-li");
        
        var oldId = parseInt( wrap.attr('data-slide') );
        var newId = parseInt( id.replace('item-', '') );

        if (oldId==1 && newId==1)
        {
            $("#slide-1 .line-open a").trigger("click");
            return false;
        }

		doc.find(".slide-menu-min a").removeClass("active");
                
        preperSlide(newId,function(){
            animateSlide(oldId,newId,function(){
                wrap.attr('data-slide',newId);
                startSlide(newId,function(){
                    wrap.removeClass('animate');
                    if(newId == 2){
                     
                    }
                    
                    if(newId == 1)
                    {
                        if($("#wrapper").attr("data-slide")=="1" )
                        {
                            $("#slide-1 .line-open a").trigger("click");
                        }
                    }

                    winResize();
                });
            });
        });
		return false;	
	});
    
    /** Buttons navigation **/
    doc.find('#slide-down').click(function(){        
        var wrap = doc.find('#wrapper');
        if(wrap.hasClass('animate')) return false;
        
        wrap.addClass('animate');
        var oldId = parseInt( wrap.attr('data-slide') );
        var newId = oldId+1;
        
            preperSlide(newId,function(){
                animateSlide(oldId,newId,function(){
                    wrap.attr('data-slide',newId);
                    startSlide(newId,function(){
                        wrap.removeClass('animate');
                        if(newId == 2){
                            
                        } else if(oldId == 3){
                            
                        }
                        winResize();
                    });
                });
            });
        
    });
    $(document).find('#slide-up').click(function(){
        var wrap = doc.find('#wrapper');
        if(wrap.hasClass('animate')) return false;
        wrap.addClass('animate');
        preperSlide(1,function(){
            animateSlide(5,1,function(){
                wrap.attr('data-slide',1);
                startSlide(1,function(){
                    wrap.removeClass('animate');
                    winResize();
                });
            });
        });
    });
    
    /** Scroll navigation **/
    
	
	
	
	/***********************/
    
   
    
    
    
    doc.find('#slide-2-side-1').click(function(){
		console.log('clicked');
        var panorama = doc.find('#slide-2 .panoram-wrap');
        doc.find('#wrapper').addClass('animate');
        doc.find('.main-nav').fadeOut(300);
        doc.find('#left-menu').fadeOut(300);
        doc.find('#top-menu').fadeOut(300);
        doc.find('.menu-top2').fadeOut(300);
        panorama.css({'z-index':100000}).animate({left:0},1500);
    });
    doc.find('.panorama-exit').click(function(){
        var panorama = doc.find('#slide-2 .panoram-wrap');
        panorama.css({'z-index':100000}).animate({left:100+'%'},1500,function(){
            doc.find('.main-nav').fadeIn(300);
            doc.find('#left-menu').fadeIn(300);
            doc.find('#top-menu').fadeIn(300);
            doc.find('.menu-top2').fadeIn(300);
            doc.find('#wrapper').removeClass('animate');
        });
        
    });
});



function winResize(){
    
    $(window).resize(function(){doWinResize();
    });
}
    function delete_cookie ( cookie_name )
	{
		var cookie_date = new Date ( );
		cookie_date.setTime ( cookie_date.getTime() - 1 );
		document.cookie = cookie_name += "=; expires=" + cookie_date.toGMTString();
	}

    function doWinResize(){

       // if(win.width()<700 || win.height()<700)
        //    return;

        doc.find('.main-slide').width(win.width()).height(win.height());
        doc.find('#wrapper').width(win.width()).height(win.height());
        
        marginTop();
       
        var chld=$("#slide-4 .wrapp").children();
        chld.attr("style","");
        $("#slide-4 .wrapp").attr("style","");

        resizeSheet();
        var curSlide = doc.find('#wrapper').attr('data-slide');
        doc.find('.main-slide').each(function(i){
            i = i+1;
            if(i != curSlide){
                $(this).css({top:win.height(),'z-index':4,overflow:'hidden'});
            }
        });
        doc.find('.sides').each(function(){
            if($(this).attr('id') == 'slide-2-side-1'){
                $(this).css({left:win.width()/3*2});
            } else if($(this).attr('id') == 'slide-2-side-2'){
                $(this).css({left:win.width()/3});
            } else if($(this).attr('id') == 'slide-2-side-3'){
                if(!doc.find('#wrapper').hasClass('slide-2-animate')){
                    $(this).css({left:0+'px'});
                }
            }
        }).width(win.width()/3);
        var slideDescHei = doc.find('#slide-4 .slide-descr').height();
       
        doc.find('#scrollbar1').tinyscrollbar_update();
        doc.find('#scrollbar2').tinyscrollbar_update();

        doc.find('#left-menu').css({'margin-top': -doc.find('#left-menu').height()/2});
        marginTop();
        updateBorders();


        var leavW=$(".sheet").width()/2;
        var winW=$(window).width()/2;

        if(winW>520)
        {
            var val=winW+leavW;
           
            doc.find('.text-slider-wrapper').css({right:val + "px"});
            doc.find('.text-slider-wrapper.right').css({left:val + "px"});
        }   

        else
        {
            doc.find('.text-slider-wrapper').css({right:634 + "px"});
            doc.find('.text-slider-wrapper.right').css({left:634 + "px"});
        }



       /* var topPos=doc.find("#wrapper").height();
        doc.find(".main-slide").each(function()
        {
            if($(this).attr("data-order")!=curSlide)
                $(this).css("top",topPos+"px");
        });

    */
    }

	 function preperSlide(number,callback){
        if(!number) return false;
        winResize();
        var slide = doc.find('.main-slide[data-order="'+number+'"]');
        if(slide.length < 1) return false;
    
        


        switch(number){
            case 1:
				slide1video = preload(1);

                slide.find("*:animated").stop(true,false);

                slide.find('.line').attr('style','').css({height:2+'px',width:0+'px',overflow:'hidden'}).removeClass('line-background');
                slide.find('.text-slider-wrapper').css({left:-slide.find('.text-slider-wrapper').width()});
                slide.find('.text-slider-wrapper.right').css({right:-slide.find('.text-slider-wrapper').width()});
                slide.find('.text-slider-buttons').fadeOut(0);
                slide.find('.line-exit').attr('style','');
                slide.find('.line-open').attr('style','');

                slide.find('.slide-1-left').css({display:'none',left:'-100%'});
                slide.find('.slide-1-right').css({display:'none',right:'-100%'});

                slide.find('.drop-2').css("height","26%");
                slide.find('.drop-2').css("bottom", "41%");

                resizeSheet();
                if(callback) callback();
				if (get_cookie ( "video" )=='no'){
				}
				else 
				{
				jQuery("#slide-1 .wrapp").css('height','200%');
				}
            break;
            case 2:
				slide2video = preload(2);
				if(callback) callback();

                //clearTimeout(animate_bars);
                //slide.find('#slide-2-side-3').removeClass("slide-2-side-3-animated");
                slide.find('.sides').stop(true,false);

                slide.find('#slide-2-side-3').find('.bar').attr('style','');
                slide.find('#slide-2-side-3').find('.bar').find('.bar-bar').attr('style','');
                slide.find('#slide-2-side-3').find('.bar').find('.text').attr('style','');
                slide.find('#slide-2-side-3').find('.bar').find('.count').html('');
                slide.find('#slide-2-side-3').find('.bar').fadeOut(0).hide();
                slide.find('.sides').each(function(){
                    if($(this).attr('id') == 'slide-2-side-3'){
                        $(this).css({left:-win.width()/3,'z-index':3});
                    } else {
                        $(this).css({left:win.width()});
                    }
                }).width(win.width()/3);
                jQuery("#slide-2 .wrapp").css('height','200%');
            break;
            case 3:
                doc.find('#wrapper').removeClass('animate-bot').removeClass('animate-top');
                preperSlide3();
                if(callback) callback();
            break;
            case 4:
				if(callback) callback();
            break;
            case 5:
                marginTop();
                if(callback) callback();
            break;
        }      
    }
    
    function get_cookie ( cookie_name )
  	{
		var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' ); 
		if ( results )
			return ( unescape ( results[2] ) );
		else
			return null;
	}
	
    function startSlide(number,callback){
        if(!number) return false;
        	if (number!=1){
	        	$(".logo").css('display','table-cell');
	        	if ($.browser.mozilla){
		        	$(".ul-top2").css('padding-left','10px');
	        	} else {
	        	$(".ul-top2").css('left','10px');
	        	}
        	} else {
	        	$(".logo").css('display','none');
	        	if ($.browser.mozilla){
		        	$(".ul-top2").css('padding-left','80px');
	        	} else {
	        	$(".ul-top2").css('left','80px');
	        	}
        	}
        	
        	if (number==5){
	        	$(".vert").css('top','0px');
        	}
        if (get_cookie ( "slide" )!=null){
        	var t1=get_cookie ( "slide" );
	    $(".ul-top2").find("."+t1).addClass("act-li");
	    doc.find(".slide-menu-min a#"+t1).addClass("active");    
        } else {
            
            if(number!=1)
                $(".ul-top2").find(".item-"+number).addClass("act-li");
        doc.find(".slide-menu-min a#item-"+number).addClass("active");
        }
        
        doc.find('#top-menu').fadeIn(300);
        doc.find('.menu-top2').fadeIn(300);
        doc.find('#footer-logo').fadeIn(300);
        var el = doc.find('#wrapper');
        var slide = doc.find('.main-slide[data-order="'+number+'"]');
        if(slide.length < 1) return false;
        switch(number){
            case 1:
              //  if(slide2video) slide2video.videobackground('destroy');
                
                slide.find('.line').attr('style','').css({height:2+'px',width:0+'px',overflow:'hidden'}).removeClass('line-background');
                slide.find('.line-exit').attr('style','');
                slide.find('.line-open').attr('style','');

                slide.find('.slide-1-left').css({display:'block',left:'-100%'});
                slide.find('.slide-1-right').css({display:'block',right:'-100%'});

                slide.find('.line').animate({width:'100%',overflow:'visible'},1000,function(){
                    if(callback) callback();
                    var hor = parseInt(win.width())+parseInt(668);
                    slide.find('.line').addClass('line-background').css({'background-position':'0 '+ hor+'px',overflow:'visible'});
                    slide.find('.sheet').animate({bottom:0+'px'},1000,function(){
                        slide.find('.line-open').delay(1300).animate({height:32+'px'},500,function(){
                            
                        });
                    });
                    var dropB=slide.find('.sheet').height()/5;
                    
                    slide.find('.sheet').fadeIn(500);
                    /*slide.find('.drop').hide().css("bottom",dropB+"px").fadeIn(1000,function(){
                        slide.find('.drop').animate({bottom:0+'px'},1500);  
                    });*/
                    slide.find('.drop-2').delay(500).animate({height:"+=30%",bottom:"-=30%"},2500);
                    

                    slide.find('.drop').hide().css("bottom",0+"px").fadeIn(1000);
                });
            break;
            case 2:
				if(slide1video) slide1video.videobackground('destroy');
				if(callback) callback();
                

                for (var i = 0; i < timeouts.length; i++)
                {
                    clearTimeout(timeouts[i]);
                }

                var ind=slide.find('.switcher a.active').index();
                slide.find("#slide-2-side-1 .bg").css("opacity","0");
                slide.find("#slide-2-side-1 .bg").eq(ind).css("opacity","1");
                //alert(ind);

					slide.find('#slide-2-side-1').css({'z-index':5}).delay(1000).animate({left:win.width()/3*2},1000,function(){
						slide.find('#slide-2-side-2').css({left:win.width()/3*2,'z-index':4}).animate({left:win.width()/3},1000,function(){
							doc.find('#scrollbar1').tinyscrollbar_update();
                            doc.find('#scrollbar2').tinyscrollbar_update();

						  
								if(slide.find('#slide-2-side-3').css('z-index') < 5 && !slide.find('#slide-2-side-3').hasClass("slide-2-side-3-animated")){
									slide.find('#slide-2-side-3').css({'z-index':5,left:-win.width()/3*2}).delay(3000).animate({left:0+'px'},1000,function(){
										el.removeClass('slide-2-animate');
										slide2left();

									});
								}
							
						});
					});

                 
              
				if(slide.find('#slide-2-side-3').css('z-index') < 5 && !slide.find('#slide-2-side-3').hasClass("slide-2-side-3-animated")){
					slide.find('#slide-2-side-3').css({'z-index':5,left:-win.width()/3*2}).delay(3000).animate({left:0+'px'},1000,function(){
						el.removeClass('slide-2-animate');                        
						slide2left();                        
					});
				}



                updateBorders();
            break;
            case 3:
                if(slide1video) slide1video.videobackground('destroy');
                if(slide2video) slide2video.videobackground('destroy');

                $("#footer-logo").hide();

                startSlide3();
                if(callback) callback();
            break;
            case 4:
                if(slide1video) slide1video.videobackground('destroy');
                if(slide2video) slide2video.videobackground('destroy');
                fixCont('#slide-4 .slider');
                if(callback) callback();
            break;
            case 5:
                if(slide1video) slide1video.videobackground('destroy');
                if(slide2video) slide2video.videobackground('destroy');
                fixCont('#slide-5 .deep-wrap');
                fixCont('#slide-5 .deep-wrap img');
                if(callback) callback();
            break;
        }
        setTimeout(function(){
            fixCont('img');
        },10);
    }




	function fixCont(id){
        jQuery(id).fadeOut(0).stop(true,true).fadeIn(1).stop(true,true);
    }
    
    function closeSlide(curSlide, callback){
        if(!curSlide) return false;
        $(".ul-top2").find("li").removeClass("act-li");
        doc.find(".slide-menu-min a").removeClass("active");
        doc.find('#footer-logo').fadeOut(300);
        doc.find('*:animated').stop(false,true);
        doc.find('#top-menu').fadeOut(300);
        doc.find('.menu-top2').fadeOut(300);
        var slide = doc.find('.main-slide[data-order="'+curSlide+'"]');

        switch(curSlide){
            case 1:
                if(callback) callback();
                slide.find('.sheet').stop(true,true).fadeOut(200);
				slide.find('.drop').hide();  
            break;
            case 2:
                if(callback) callback();
            break;
            case 3:
                $("#footer-logo").show();
                if(callback) callback();
            break;
            case 4:
                if(callback) callback();
            break;
            case 5:
                if(callback) callback();
            break;
        }
    }

    function beforePageStart(){
        doc.find('#wrapper').attr('data-slide','1').fadeOut(1).width(win.width()).height(win.height());
		jQuery('.sheet').hide();
		jQuery('.drop').hide();
        doc.find('.main-slide').each(function(i){
            i = i+1;
            $(this).attr('data-order',i).css({top:win.width(),'z-index':4,overflow:'hidden'});
            if(i == 1){
                $(this).css({top:0,'z-index':5});
            }
        }).width(win.width()).height(win.height());
        doc.find('#main-menu').height(0);
        doc.find('#left-menu').fadeOut(0);
        doc.find('#footer-logo').fadeOut(0);
    }
        
    function animateSlide(curSlide,newSlide,callback){
        if(!curSlide) return false;
        if(!newSlide) return false;
        
        var curSlideEl = doc.find('.main-slide[data-order="'+curSlide+'"]');
        var newSlideEl = doc.find('.main-slide[data-order="'+newSlide+'"]');

        var topPos=doc.find("#wrapper").height();
        doc.find(".main-slide").each(function()
        {
            if($(this).attr("data-order")==curSlide)  
                $(this).css("z-index","40");
            
            else if($(this).attr("data-order")==newSlide)  
                $(this).css("z-index","40");    

            else
                $(this).css("z-index","4");                    


        });

        var curTop = -win.height();
        var newTop = win.height();
        if(newSlide < curSlide){
            curTop = win.height();
            newTop = -win.height();
        }
        closeSlide(curSlide,function(){
            curSlideEl.animate({top:curTop},1000,"easeInOutCubic",function(){
                curSlideEl.css({top:win.height()});
            });
            newSlideEl.css({top:newTop}).animate({top:0+'px'},1000,"easeInOutCubic",function(){
                if(callback) callback();
                
            })
            
        })
        
    }
	
    function resizeSheet(){
	if(curSlide==1)
	{
        var wwidth = win.width();
        var wheight = win.height();


        if(wwidth > 1700)
        {
            $(".line-open").css("left","52%");
        }
        else
        {
            $(".line-open").css("left","50%");   
        }

        var sheet = doc.find('.sheet');
        var prop = sheet.height()/sheet.width();        

        var newHeight = wheight * 0.72;


        var newWidth = newHeight/prop;

        if(wwidth<1000)
            sheet.height(newHeight).width(newWidth).css({left:'500px','margin-left':-newWidth/2});
        else    
            sheet.height(newHeight).width(newWidth).css({left:'50%','margin-left':-newWidth/2});

        if(wheight<600)
        {
            
            sheet.height(404+"px");
            sheet.width(404/prop+"px");
            sheet.css('margin-left',-  404/prop /2);
        }
        
	}
}

    
    function slide2left(){
        var slide = doc.find('#slide-2').find('#slide-2-side-3');
        var bars = slide.find('.bar');
        var count_bar = bars.length;

        var count = 1;

        if(slide.hasClass("slide-2-side-3-animated"))
            return;
        else
        {
            clearTimeout(animate_bars);
            
            slide.find('.bar').attr('style','');
            slide.find('.bar').find('.bar-bar').attr('style','');
            slide.find('.bar').find('.text').attr('style','');
            slide.find('.bar').find('.count').html('');
            slide.find('.bar').fadeOut(0).hide();
            

            slide.addClass("slide-2-side-3-animated")
        }
        
        bars.show();

        slide.find('.bar img').fadeOut(0);
        slide.find('.bar .text').fadeOut(0);
        slide.find('.bar .count').html("");


        animate_bars = setInterval(function(){
        if(count >= count_bar){ 
            slide.removeClass("slide-2-side-3-animated");
            clearInterval(animate_bars);
        }
        
        slide.find('.bar[data-order="'+count+'"]').find('.bar-bar').animate({top:'0%',opacity:1},1000);
        slide.find('.bar[data-order="'+count+'"]').find('.text').animate({opacity:1},1000);
        slide.find('.bar[data-order="'+count+'"]').find('.bar-bar').find('img').fadeOut(0).fadeIn(1000);
            countBars(count);
            count = parseInt(count) + parseInt(1);

            if(count==5)
            {
                slide.find('.bar[data-order="'+4+'"]').css("overflow","visible");
            }
        }, 1000);

        slide.find('.bar .lake').delay("5000").fadeIn(400);

    }
                            
    function countBars(count) {
        var current = 0;
        var slide = doc.find('#slide-2').find('#slide-2-side-3')
        var finish = slide.find('.bar[data-order="'+count+'"]').attr('data-count');
        var miliseconds = 1000;
        var rate = 10;
                                    
        var counter = setInterval(function(){
            if(current >= finish) clearInterval(counter);
            slide.find('.bar[data-order="'+count+'"]').find('.count').html(current);
            current = parseInt(current) + parseInt(rate);
            if(current >= finish) {
                slide.find('.bar[data-order="'+count+'"]').find('.count').html(finish);
            } 
        }, miliseconds / (finish / rate));
    }
    


function preload(currnetVideo)
{

    if(detectmob())
    {
        return null;
    }

	var _loal_var = '';
	if (get_cookie ( "video" )=='no'){
		//_loal_var=jQuery("#slide-"+currnetVideo+" .wrapp").css({'background':'url(/video/slide-'+currnetVideo+'.jpg)'});
		/*_loal_var=jQuery("#slide-"+currnetVideo+" .wrapp img").css('width','100%');
		_loal_var=jQuery("#slide-"+currnetVideo+" .wrapp img").css('height','100%');*/
		
		_loal_var = jQuery('#slide-'+currnetVideo+' .wrapp').videobackground({
			videoSource: [
				[/*'/video/slide-'+currnetVideo+'.mp4', 'video/mp4'*/],
				[/*'/video/slide-'+currnetVideo+'.ogv', 'video/ogg'*/],
				[/*'/video/slide-'+currnetVideo+'.webm', 'video/webm'*/],
			],
			controlPosition: null,
			loop:true,
			poster: '/img/slide-'+currnetVideo+'.jpg',
		   
			preloadCallback: function(){
			
			},
			loadedCallback: function() {
				if(currnetVideo !=1)
				{
					var el = $(this);
					var vhei = win.height();
					var vwid = vhei * videoProp;
					if(vwid < win.width()) {
						vwid = win.width();
						vhei = win.width() / videoProp;

					}
			   }
				/*el.videobackground('mute').videobackground('play');*/
				
			}
		});
		
		winResize();
	}
	else 
	{
		_loal_var = jQuery('#slide-'+currnetVideo+' .wrapp').videobackground({
			videoSource: [
				['/video/slide-'+currnetVideo+'.mp4', 'video/mp4'],
				['/video/slide-'+currnetVideo+'.ogv', 'video/ogg'],
				['/video/slide-'+currnetVideo+'.webm', 'video/webm'],
			],
			controlPosition: null,
			loop:true,
			poster: '/img/slide-'+currnetVideo+'.jpg',
		   
			preloadCallback: function(){
			
			},
			loadedCallback: function() {
				if(currnetVideo !=1)
				{
					var el = $(this);
					var vhei = win.height();
					var vwid = vhei * videoProp;
					if(vwid < win.width()) {
						vwid = win.width();
						vhei = win.width() / videoProp;

					}
			   }
				/*el.videobackground('mute').videobackground('play');*/
				
			}
		});
	}
	return _loal_var;
}




function start_play(currnetVideo)
{
	if (get_cookie ( "video" )=='no'){
	}
	else 
	{
		if(currnetVideo == 2)
		{
			slide2video.videobackground('mute').videobackground('play');
		}
		if(currnetVideo==1)
		{
			slide1video.videobackground('mute').videobackground('play');
		}
	}
}
function first_load()
{
	slide1video = preload(1);
	slide2video = preload(2);
	jQuery("#slide-2-side-1").prepend(_lang_pan);
	jQuery("#slide-1 .wrapp").css('height','200%');
	if (get_cookie ( "video" )=='no'){
		jQuery("#slide-2 .pano-borders").remove();
		jQuery("#slide-2 .show-pan").remove();
	}
	else 
	{
		jQuery("#audio").html('<audio id="myTune" controls loop class><source src="/audio/MUSIC_30s.mp3" type="audio/mpeg"><source src="/audio/mus.ogg" type="audio/ogg;" ></audio>');
       // jQuery("#slide-2 .pano-borders").html('<div class="panoram">  <img src="/templates/morosha/images/panorama/panorama.jpg">          <div class="help"></div>          </div>');
		//jQuery("#slide-2-side-1").prepend(_lang_pan);
		//jQuery("#slide-1 .wrapp").css('height','200%');
		
		if (get_cookie ( "sound" )=='no'){
			$("#myTune").addClass("stopped");
			$("#sound").addClass('nosond');
			document.getElementById('myTune').pause();
		}
		else 
		{	
			document.getElementById('myTune').play();
		}
        
	}

	done();
}

function done()
{
	$('.pop').remove();
	delays = 1000;
	fadeOuts = 500;
	fadeIns = 500;
	
    doc.find('.loading-img').delay(delays).fadeOut(fadeOuts);
	doc.find('#loading').delay(delays).fadeOut(fadeOuts,function(){
		doc.find('#wrapper').fadeIn(fadeIns,function(){
            doc.find('.loading-img').remove();
			doc.find('#loading').remove();
			_loaded = 1;
			doc.find('.pop').remove();
			doc.find('#scrollbar1').tinyscrollbar();
            doc.find('#scrollbar2').tinyscrollbar();
			startSlide(curSlide,function(){
				doc.find('#main-menu').height(0);
				doc.find('#left-menu').css({'margin-top': -doc.find('#left-menu').height()/2}).fadeIn(500);
				doc.find('#footer-logo').fadeIn(500);
			});
		});
	});
	resizeSheet();
    marginTop();
    //winResize();
	
	
	setTimeout('startscroll();',delays+fadeOuts+fadeIns);
	
}


function startscroll()
{
	doc.find('#scrollbar1').mousewheel(function(event, delta,deltaX, deltaY)
    {

        if($(this).find(".scrollbar").hasClass("disable"))
        {   
             $("body").trigger(event,delta, deltaX, delta);
             return true;
        }

        else
        {
            return false;   
        }
        


    });


     /** Scroll navigation **/
    doc.find('#scrollbar2').mousewheel(function(event, delta,deltaX, deltaY)
    {

        if($(this).find(".scrollbar").hasClass("disable"))
        {   
             $("body").trigger(event,delta, deltaX, delta);
             return true;
        }

        else
        {
            return false;   
        }
        


    });

    doc.find('body').mousewheel(function(event, delta, deltaX, deltaY) {
        
        var el = doc.find('#wrapper');
        
        if( el.find('#scrollbar1').is(':hover')  && !el.find('#scrollbar1 .scrollbar').hasClass('disable')) return false;
        if( el.find('#scrollbar2').is(':hover')  && !el.find('#scrollbar2 .scrollbar').hasClass('disable')) return false;

        if( el.hasClass('animate') ) return false;
        el.addClass('animate');
        
        var curSlide = parseInt(el.attr('data-slide'));
        var totSlide = el.find('.main-slide').length;
        var newSlide = false;
        if(delta > 0 && curSlide != 1){
            newSlide = curSlide-1;
            if(el.hasClass('slide-2-animate')){
                startSlide(curSlide,function(){
                    el.removeClass('animate').removeClass('slide-2-animate');
                    winResize();
                });
            } else {
                
                preperSlide(newSlide,function(){
                    animateSlide(curSlide,newSlide,function(){
                        el.attr('data-slide',newSlide);
                        startSlide(newSlide,function(){
                            el.removeClass('animate');
                            if(newSlide == 2){
                             
                            } else if(curSlide == 3){
                                el.addClass('animate-top');
                            }
                            winResize();
                        });
                    });
                });
            }
            
            
        } else if (delta < 0 && curSlide != totSlide) {

            newSlide = curSlide+1;
            if(el.hasClass('slide-2-animate')){
                startSlide(curSlide,function(){
                    el.removeClass('animate').removeClass('slide-2-animate');
                    winResize();
                });
            } else {
               
                preperSlide(newSlide,function(){
                    animateSlide(curSlide,newSlide,function(){
                        el.attr('data-slide',newSlide);
                        startSlide(newSlide,function(){
                            el.removeClass('animate');
                            if(newSlide == 2){
                             
                            } else if(curSlide == 3){
                                el.addClass('animate-bot');
                            }
                            winResize();
                        });
                    });
                });
            }            
        } else if (delta < 0 && curSlide == totSlide){
            el.removeClass('animate');
            //location.replace('/media-arhiv.html');
        }else {
            el.removeClass('animate');
        }
    });

}

function get_cookie ( cookie_name )
{
	var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' ); 
	if ( results )
		return ( unescape ( results[2] ) );
	else
		return null;
}

function clicked(){
        var panorama = doc.find('#slide-2 .panoram-wrap');
        doc.find('#wrapper').addClass('animate');
        doc.find('.main-nav').fadeOut(300);
        doc.find('#left-menu').fadeOut(300);
        doc.find('#top-menu').fadeOut(300);
        doc.find('.menu-top2').fadeOut(300);

        updateBorders();
        
        panorama.css({'z-index':100000}).animate({left:0},1500);
		panorama.find(".help").css("left",$(window).width()/2 + "px");
		panorama.find(".help_ru").css("left",$(window).width()/2 + "px");
		panorama.find(".help_en").css("left",$(window).width()/2 + "px");

		$(".panoram-wrap .left").mousehold(function()
		{

			var lef=parseInt($(".panoram-wrap .panoram").css("left"));
			lef+=10;

			var w=$(".panoram-wrap .panoram").width();
			var winW=$(window).width();
			var val=2*w - winW;
			var offLeft=-w+winW;
			offLeft=offLeft*(-1);

			$(".right").show();
			if(lef>=offLeft)
			{
				$(".left").hide();
				return false;
			}

			$(".panoram-wrap .panoram").stop().animate({"left":lef},100);



			if($(".panoram-wrap .help").is(":visible"))
				{
					$(".panoram-wrap .help").fadeOut("slow");	
				}		
		});

		$(".panoram-wrap .right").mousehold(function()
		{
			var lef=parseInt($(".panoram-wrap .panoram").css("left"));
			lef-=10;
			$(".left").show();
			if(lef<=2)
			{
				$(".right").hide();
				return false;
			}


			$(".panoram-wrap .panoram").stop().animate({"left":lef},100);



			if($(".panoram-wrap .help").is(":visible"))
				{
					$(".panoram-wrap .help").fadeOut("slow");	
				}		


		});
		$(".panoram-wrap .panoram").draggable({ 
			axis: "x",
			//containment : [-226616,0,0,0],
			containment : 'parent',
			drag: function( event, ui ) {
                var bord=Math.abs( parseInt($(".pano-borders").css("left")));
                var leftP=parseInt($(".panoram-wrap .panoram").css("left"));
                if(leftP>bord)
                {
                    $(".panoram-wrap .panoram").css("left",bord-3);
                    return false;
                }

				updateArrows();
				if($(".panoram-wrap .help").is(":visible"))
				{
					$(".panoram-wrap .help").fadeOut("slow");	
				}		
			}
		});


    }
