
$(window).load(function()
{
    var slide_height = $('#slide-4').height();
    var slide_width  = $('#slide-4').width();
    if(slide_width < 1400) {
        $('#slide-4').find('.slide-descr-wrapp h1').css('font-size','25px');
    }    
    //$('#slide-4').find('.slide-img').height(slide_height-308);
    
    $('#slide-4').find('.slide').each(function(i){
        $(this).attr('data-order',i);
        if(i>0){
            $(this).css({left:'100%'});
            $(this).find('.slide-img img').fadeOut(10);
        } else {
            $(this).addClass('selected');
        }
    });
    var slide_leng = $('#slide-4').find('.slide').length;
    var slide_par = $('#slide-4').find('.slider');
    
    $('#slide-4').find('.photo-left').click(function(){
        if(slide_par.hasClass('animate')) return false;
        slide_par.addClass('animate');
        var cur_el = $('#slide-4').find('.slide.selected').attr('data-order');
        
        var new_el = parseInt(cur_el)-1;
        if(new_el < 0){
            new_el = slide_leng-1;
        }
        ChangeSlide(cur_el,new_el,'left',function(){
            slide_par.removeClass('animate');
        });
        return false;
    });
    $('#slide-4').find('.photo-right').click(function(){
        if(slide_par.hasClass('animate')) return false;
        slide_par.addClass('animate');
        var cur_el = $('#slide-4').find('.slide.selected').attr('data-order');
        
        var new_el = parseInt(cur_el)+1;
        if(new_el > slide_leng-1){
            new_el = 0;
        }
        ChangeSlide(cur_el,new_el,'right',function(){
            slide_par.removeClass('animate');
        });
        return false;
    });
    
   function ChangeSlide(oldSlide,newSlide,wey,callback){
        var left1 = '-100%';
        var left2 = '100%';
        if(wey != 'left'){
            left1 = '100%';
            left2 = '-100%';
        }
       $('#slide-4').find('.slide[data-order="'+oldSlide+'"]').removeClass('selected').animate({left:left1},1500).find('.slide-img img').fadeOut(1500);
        $('#slide-4').find('.slide[data-order="'+newSlide+'"]').find('.slide-img img').fadeIn(1500);
        $('#slide-4').find('.slide[data-order="'+newSlide+'"]').addClass('selected').css('left',left2).animate({left:'0%'},1500,function(){
            if(callback) callback();
        })
        
    }
});