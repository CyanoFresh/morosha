jQuery(function($){
    
    /******** SLIDE 1 ********/
    
    var text_silde_leng = $('#slide-1 .single-text-slide').length;
    $('#slide-1 .single-text-slide').each(function(i){
        $(this).attr('data-order',i);
        
        if(i!=0){
            $(this).css({left:'100%'});
            $('#slide-1 .text-slider-buttons ul').append('<li data-order="'+i+'"></li>');
        } else {
            $('#slide-1 .text-slider-buttons ul').append('<li data-order="'+i+'" class="selected"></li>');
        }
        if (i == text_silde_leng-1){
            $('#slide-1 .text-slider-buttons ul').append('<li class="next"></li>');
            $('#slide-1 .text-slider-buttons ul').prepend('<li class="prev"></li>');
        }
    })
    $('#slide-1 .text-slider-buttons ul').find('li').click(function(){
        var par = $('#slide-1 .text-slider-buttons ul');
        if(par.hasClass('animate')) return false;
        //par.addClass('animate');
        var old_val = $('#slide-1 .text-slider-buttons ul').find('li.selected').attr('data-order');
        var new_val = $(this).attr('data-order');
        if(new_val && new_val == old_val) return false;
        var animate = 'fade';
        if($(this).hasClass('next')){
            if(old_val == text_silde_leng-1){
                new_val = 0;
            } else {
                new_val = parseInt(old_val)+1;
            }
            animate = 'right';
        } else if($(this).hasClass('prev')){
            if(old_val == 0){
                new_val = text_silde_leng-1;
            } else {
                new_val = parseInt(old_val)-1;
            }
            animate = 'left';
        } else {
            if(new_val > old_val){
                animate = 'right';
            } else {
                animate = 'left';
            }
        }
        TextSliderChange(old_val,new_val,animate,function(){
            par.removeClass('animate');
        })
        return false;
    });
    $('#slide-1 .line-open a').click(function(){
        $(".ul-top2").find(".item-1").addClass("act-li");

        var par = $(this).parents('.line');
        var slide_length = par.find('.single-text-slide').length;
        var slide_width = par.find('.text-slider-wrapper').outerWidth();
        par.find('.line-open').animate({height:'0px'},1000,function(){
            par.find('.line-open').hide();
            par_height = '336px';
            par.addClass('line-background').css('background-position','200% bottom').animate({height:par_height,top:'30%'},1000,function(){
                var backgrWidth = 668/(329/(par.height()*0.95));
                var posX = 100-(backgrWidth/(slide_width/100))/2;
                if(posX < 80 || posX > 100){
                    posX = '100%'
                } else {
                    posX = posX+'%';
                }
                par.animate({'background-position-x': posX},1000,function(){
                    par.css('background-position', posX+' bottom')
                });


                par.find('.slide-1-left').css({display:'block',left:'-100%'}).animate({'left': 0},1000);
                par.find('.slide-1-right').css({display:'block',right:'-100%'}).animate({'right': 0},1000);

                par.find('.text-slider-buttons').fadeOut(0);
                
                var leavW=$(".sheet").width()/2;
                var winW=$(window).width()/2;

                par.find('.text-slider-wrapper').css({display:'block',left:'auto',right:'100%'}).animate({right:winW+leavW + "px"},1500,function(){
                    par.find('.text-slider-buttons').fadeIn(1000);
                });

                par.find('.text-slider-wrapper.right').stop(true,true).css({display:'block',left:'100%',right:"auto"}).animate({left:winW+leavW + "px"},1500);
            });
            par.find('.line-exit').show(1000);
        });
        return false;
    });
    $('#slide-1 .line-exit a').click(function(){
        $(".ul-top2").find(".item-1").removeClass("act-li");

        var par = $(this).parents('.line');
        par.find('.text-slider-wrapper').animate({left:'-100%'},2500);
        par.find('.text-slider-wrapper.right').stop(true,true).css({left:"auto"}).animate({right:'-100%'},2500);

        par.find('.slide-1-left').animate({'left': "-100%"},1000);

        par.find('.slide-1-right').animate({'right': "-100%"},1000,function(){
            par.animate({height:'2px',top:'55%'},1500,function(){
                par.css('background-position', 'right bottom');
                par.removeClass('line-background');
                par.find('.line-exit').hide(0);
                par.find('.line-open').show(0).animate({height:'32px'},1500);
            });
        });
        
        return false;
    });
    
    function TextSliderChange(old_val,new_val,effect,callback){
        $('#slide-1 .text-slider-buttons ul').find('li[data-order="'+old_val+'"]').removeClass('selected');
        $('#slide-1 .text-slider-buttons ul').find('li[data-order="'+new_val+'"]').addClass('selected');
        var left1 = '-100%';
        var left2 = '100%';
        if(effect == 'right') {
            left1 = '100%';
            left2 = '-100%';
        }
        $('#slide-1 .text-slider').find('.single-text-slide[data-order="'+old_val+'"]').animate({left:left1},1000);
        $('#slide-1 .text-slider').find('.single-text-slide[data-order="'+new_val+'"]').css({left:left2}).animate({left:'0%'},1000,function(){
            if(callback) callback();
        });
    }
    
    function resizeEl(el,wwidth,wheight,bwidth,bheight){
        if( !el || !el.width() || !el.height() || el.width() == 0 || el.height() == 0 ) return false;
        if(!wwidth) wwidth = $(window).width();//el.parents('.main-slide').width();
        if(!wheight) wheight = $(window).height()-192;//el.parents('.main-slide').height()-35;
        if(!bwidth) bwidth = 1600;
        if(!bheight) bheight = 800;
        width = el.width();
        height = el.height();
        propW = bwidth/width;
        propH = bheight/height;
        elwidth = wwidth/propW;
        elheight = wheight/propH;
        el.width(elwidth).height($(window).height()-192).css({left:'50%','margin-left':-(elwidth/2)});
    }
    
    /******** / SLIDE 1 ********/
});