var deepTops=[340,233,123,15,-46];
var deeps=[470,630,850,989,1050];
var lineHeights=[384,275,168,59,0];
var steps=[1.4,1.69,1.6,1.6];
var currentDeepNum=0;

$(window).load(function()
{

	$("#slide-5 .deep-slider").css("top",deepTops[0]+"px");
	$("#slide-5 .line").css("height",lineHeights[0]+"px");

	marginTop();
	$(window).resize(function()
	{
		marginTop();
	});

	//------init height slide-3--------
	var h=parseFloat($("#slide-5 #bg").eq(0).height());
	var hw=parseFloat($(window).height());
	h=h<hw ? h : hw;	
	//$("#slide-5").css("height",h);
	// --------/init height slide-3-------

	$("#slide-5 .tabs a").click(function(event,isFromSlider)
	{
		if($(this).hasClass("active")) return false;
		if($("#slide-5 .deep-slider").is(":animated")) return false;


		$("#slide-5 .tabs a").removeClass("active");	
		$(this).addClass("active");
		var ind=$(this).index();

		$("#slide-5 .bottle").each(function()
		{
			if($(this).is(":visible"))
			{
				
				$(this).fadeOut("fast",function()
				{				

					$("#slide-5 .bottle").eq(ind).fadeIn("fast");	
					$("#slide-5 #bg img").fadeOut(500).eq(ind).fadeIn(500);
					currentDeepNum=ind;	
					if(!isFromSlider)					
						stopDrag(false);
				});		

				return;	
			}
		});		
	});

// --------drag slider-------	
	$("#slide-5 .deep-slider").draggable({ 
		axis: "y",
		drag: function( event, ui ) {
			var top=parseInt($("#slide-5 .deep-slider").css("top"));

			if(top<-47)  // top border
			{
				$("#slide-5 .deep-slider").css("top",-46);
				return false;
			}

			if(top>341)  // bottom border
			{
				$("#slide-5 .deep-slider").css("top",340);
				return false;
			}

			var mul;
			var cur=parseInt($("#slide-5 .deep-slider .box span").text());


			if(top <= deepTops[currentDeepNum])	{mul=3.5;}
			else if(top > deepTops[currentDeepNum]){ mul=-3.5;}

			var cur=parseInt($("#slide-5 .deep-slider .box span").text());
			cur+=mul;
			$("#slide-5 .deep-slider .box span").text(parseInt(cur));
			

		},

		stop:function( event, ui ) {
			stopDrag(true);
		}


		 });

	// --------/drag slider-------	
	

	$(".active-area").click(function()
	{
		var indArea=$(this).index();
		$("#slide-5 .tabs a").eq(indArea).trigger("click",[false]);
	});

	// ---win resize---
	$(window).resize(function()
	{
		var h=parseFloat($("#slide-5 #bg").eq(0).height());
		var hw=parseFloat($(window).height());
		h=h<hw ? h : hw;	
		//$("#slide-5").css("height",h);
		
	});
	// ---/win resize---
});

function stopDrag (isFromSlider)
{

			var top=parseInt($("#slide-5 .deep-slider").css("top"));
			
			var oldCurrent=currentDeepNum;

			var min=9999;
			if(isFromSlider)
			for(i=0;i<5;i++)
			{
				diff=Math.abs(deepTops[i]-top);
				if(diff<min && i!=oldCurrent)
				{
					min=diff;
					currentDeepNum=i;
				}				
			}
			
			$("#slide-5 .line").animate({"height":lineHeights[currentDeepNum]},500);

			$("#slide-5 .deep-slider").animate({"top":deepTops[currentDeepNum]},500,function()
				{
					$("#slide-5 .deep-slider").removeAttr("id");
					$("#slide-5 .deep-slider").attr("id","slider-"+ (currentDeepNum+1));

					if(isFromSlider)
					{
						$("#slide-5 .tabs a").eq(currentDeepNum).trigger("click",[isFromSlider]);						
					}

				});			
			count(deeps[currentDeepNum]);
};

function count (to) {
			var $ele = $("#slide-5 .deep-slider .box span"),
            num = parseInt($ele.html()),
            up = to > num,
            num_interval = Math.abs(num - to) / 40;
            num_interval=num_interval<1? 1 : num_interval;		
			
        var loop = function() {
            num = Math.floor(up ? num+num_interval: num-num_interval);
            if ( (up && num > to) || (!up && num < to) ) {
            	                //console.log("CLEAR!");
                num = to;
                clearInterval(animation)

            }
            $ele.html(num);
        }

        var animation = setInterval(loop, 1);
};


function marginTop ()
{
	var h= parseInt($(window).height()) / 2;
	var hE=parseInt($("#slide-5 .deep-wrap").height()) / 2;
	var top=h-310 - 30;


	if (top<0 ) top=0;
	$("#slide-5 .deep-wrap").css("marginTop",top +"px");

};