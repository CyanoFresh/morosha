$(document).ready(function()
{
	$("#slide-2 .switcher a").click(function()
	{
		var ind=$(this).index();
		$("#slide-2 .switcher a").removeClass("active");
		$(this).addClass("active");

		$("#slide-2	.flexcroll").hide();
		$("#slide-2	.flexcroll").eq(ind).show();
		$("#slide-2	.flexcroll").tinyscrollbar_update();

        $("#slide-2 #slide-2-side-1 .bg").hide().css("opacity","1");
        $("#slide-2 #slide-2-side-1 .bg").eq(ind).show();
	
        $(".panoram-wrap .panoram img").hide();
		$(".panoram-wrap .panoram img").eq(ind).show();
		/*
		if(ind==1){
			//$(".panoram-wrap").hide();
			jQuery("#slide-2 .show-pan").parent().hide();
		}else{ 
			//$(".panoram-wrap").show();
			jQuery("#slide-2 .show-pan").parent().show();
		}*/
	});
	
	$("#slide-2 #slide-2-side-1 .bg").click(function(e)
	{
		return false;
	});
});